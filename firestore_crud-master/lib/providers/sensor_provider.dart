import 'package:firestore_crud/models/sensor.dart';
import 'package:firestore_crud/services/firestore_service.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class SensorProvider with ChangeNotifier {
  final firestoreService = FirestoreService();
  String _name;
  bool _status;
  String _sensorId;
  var uuid = Uuid();

  //Getters
  String get name => _name;
  bool get status => _status;

  //Setters
  changeName(String value) {
    _name = value;
    notifyListeners();
  }

  changeStatus(bool value) {
    _status = value;
    notifyListeners();
  }

  loadValues(Sensor sensor){
    _name=sensor.name;
    _status=sensor.status;
    _sensorId=sensor.sensorId;
  }


  saveSensor() {
    print(_sensorId);
    if (_sensorId == null) {
      var newSensor = Sensor(name: name, status: status, sensorId: uuid.v4());
      firestoreService.saveSensor(newSensor);
    } else {
      //Update
      var updatedSensor =
          Sensor(name: name, status: _status, sensorId: _sensorId);
      firestoreService.saveSensor(updatedSensor);
    }
  }

  removeSensor(String sensorId){
    firestoreService.removeSensor(sensorId);
  }

}
