import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firestore_crud/models/sensor.dart';

class FirestoreService {
  FirebaseFirestore _db = FirebaseFirestore.instance;

  Future<void> saveSensor(Sensor sensor){
    return _db.collection('sensors').doc(sensor.sensorId).set(sensor.toMap());
  }

  Stream<List<Sensor>> getSensors(){
    return _db.collection('sensors').snapshots().map((snapshot) => snapshot.docs.map((document) => Sensor.fromFirestore(document.data())).toList());
  }

  Future<void> removeSensor(String sensorId){
    return _db.collection('sensors').doc(sensorId).delete();
  }



}