import 'package:firestore_crud/screens/edit_sensor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../models/sensor.dart';

class Sensors extends StatelessWidget {
  get text => null;

  get subtitle => null;

  set subtitle(bool s)
  {
    s= subtitle;
  }
  @override
  Widget build(BuildContext context) {
    final sensors = Provider.of<List<Sensor>>(context);


    return Scaffold(
        appBar: AppBar(
          title: Text('Sensors'),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.add,
                size: 30.0,
              ),
              onPressed: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => EditSensor()));
              },
            )
          ],
        ),


        body: (sensors != null)
            ? ListView.builder(
                itemCount: sensors.length,
                itemBuilder: (context, index)
                {
                  return ListTile(
                    title: Text(sensors[index].name),
                    subtitle: Text(sensors[index].status.toString()),
          trailing:  Switch(

            value: subtitle,

            onChanged: (value) {
              setState(() {
                subtitle = value;

                print(subtitle);
              });
            },
            activeTrackColor: Colors.lightGreenAccent,
            activeColor: Colors.green,
          ),);
                })
            : Center(child: CircularProgressIndicator()));

  }

  void setState(Null Function() param0) {}

}

 /*class SwitchScreen extends StatefulWidget   {
  @override
  SwitchClass createState() => new SwitchClass();

  Widget build(BuildContext context) {
    final sensors = Provider.of<List<Sensor>>(context);}

  }

  class SwitchClass extends State {
  bool status = false;
 //var textValue = 'disable';
 //var textValue= Text(status);

  void toggleSwitch(bool value) {

  if(status == false)
  {
  setState(() {
  status = true;
  //textValue = 'enable';
  });

  }
  else
  {
  setState(() {
  status = false;
 // textValue = 'disable';
  });
  }
  }
  @override
  Widget build(BuildContext context) {
  return Column(
  mainAxisAlignment: MainAxisAlignment.center,
  children:[ Transform.scale(
  scale: 1,
  child: Switch(
  onChanged: toggleSwitch,
  //value: status,
  activeColor: Colors.blue,
  activeTrackColor: Colors.yellow,
  inactiveThumbColor: Colors.redAccent,
  inactiveTrackColor: Colors.orange,
  )
  ),
 // Text('$textValue', style: TextStyle(fontSize: 20),)
  ]);
  }}*/
