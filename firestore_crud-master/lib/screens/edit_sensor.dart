import 'package:firestore_crud/models/sensor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firestore_crud/providers/sensor_provider.dart';

class EditSensor extends StatefulWidget {
  final Sensor sensor;

  EditSensor([this.sensor]);

  @override
  _EditSensorState createState() => _EditSensorState();
}

class _EditSensorState extends State<EditSensor> {
  final nameController = TextEditingController();
  final statusController = TextEditingController();

  @override
  void dispose() {
    nameController.dispose();
    statusController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    if (widget.sensor == null) {
      //New Record
      nameController.text = "";
      statusController.text = "";
      new Future.delayed(Duration.zero, () {
        final sensorProvider = Provider.of<SensorProvider>(context,listen: false);
        sensorProvider.loadValues(Sensor());
      });
    } else {
      //Controller Update
      nameController.text=widget.sensor.name;
      statusController.text=widget.sensor.status.toString();
      //State Update
      new Future.delayed(Duration.zero, () {
        final sensorProvider = Provider.of<SensorProvider>(context,listen: false);
        sensorProvider.loadValues(widget.sensor);
      });
      
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final sensorProvider = Provider.of<SensorProvider>(context);

    return Scaffold(
      appBar: AppBar(title: Text('Edit Sensor')),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: <Widget>[
            TextField(
              controller: nameController,
              decoration: InputDecoration(hintText: 'Sensor Name'),
              onChanged: (value) {
                sensorProvider.changeName(value);
              },
            ),
            TextField(
              controller: statusController,
              decoration: InputDecoration(hintText: 'Sensor Status'),
                onChanged:(value)
              {
                sensorProvider.changeStatus(value=='on'? true : false);

              },
            ),
            SizedBox(
              height: 20.0,
            ),
            RaisedButton(
              child: Text('Save'),
              onPressed: () {
                sensorProvider.saveSensor();
                Navigator.of(context).pop();
              },
            ),
            (widget.sensor !=null) ? RaisedButton(
              color: Colors.red,
              textColor: Colors.white,
              child: Text('Delete'),
              onPressed: () {
                sensorProvider.removeSensor(widget.sensor.sensorId);
                Navigator.of(context).pop();
              },
            ): Container(),
          ],
        ),
      ),
    );
  }
}

class sensorProvider {
}

