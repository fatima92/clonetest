class Sensor{
  final String sensorId;
  final String name;
   final bool status;

  Sensor({this.sensorId,this.status, this.name});

  Map<String, dynamic> toMap(){
    return {
      'sensorId' : sensorId,
      'name' : name,
      'status' : status
    };
  }

  Sensor.fromFirestore(Map<String, dynamic> firestore)
      : sensorId = firestore['sensorId'],
        name = firestore['name'],
        status = firestore['status']==("on")?true : false;
}