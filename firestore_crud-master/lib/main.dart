import 'package:firebase_core/firebase_core.dart';
import 'package:firestore_crud/models/sensor.dart';
import 'package:firestore_crud/providers/sensor_provider.dart';
import 'package:firestore_crud/screens/sensors.dart';
import 'package:firestore_crud/services/firestore_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final firestoreService = FirestoreService();

    return MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => SensorProvider()),
            StreamProvider(create: (context)=> firestoreService.getSensors()),
          ],
          child: MaterialApp(
        title: 'Sensor Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Sensors(),
      ),
    );
  }
}


  @override
  Widget build(BuildContext context) {


}